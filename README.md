**DATABASE SIMULATION ENGINE**

A Database Simulation Engine which builds index for a given data, and converts query to relational algebraic expressions and executes the algebraic expressions and provides results for the query. 
The query evaluation engine contains implementation of different Join , Sort and Grouping Algorithms which is executed dynamically depending on how the data or index is stored.

Mail any questions to: sabharin@buffalo.edu